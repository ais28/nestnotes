import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ProductsModule } from './products/products.module';
import { CategoriesController } from './products/controllers/categories.controller';

@Module({
  imports: [UsersModule, ProductsModule],
  controllers: [AppController, CategoriesController],
  providers: [AppService],
})
export class AppModule {}
